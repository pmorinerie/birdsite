const TWITTER_API_URL    = 'https://api.twitter.com/';
const TWITTER_UPLOAD_URL = 'https://upload.twitter.com/';

// Thin wrapper around the Twitter HTTP API.
//
// Intended to be called from the background script.
class TwitterApi {
  constructor(consumerKey, consumerSecret) {
    this.consumer_key    = TWITTER_BIRDSITE_CONSUMER_KEY;
    this.consumer_secret = TWITTER_BIRDSITE_CONSUMER_SECRET;
    this.credentials = null;
  }

  // Set credentials if you want to perform an authenticated request.
  // Otherwise, default empty credentials are used.
  setCredentials(twitterCredentials) {
    this.credentials = twitterCredentials;
  }

  async sendRequest(path, method = 'GET', params = {}) {
    // Figure out API host
    let baseUrl = path.match(/upload/) ? TWITTER_UPLOAD_URL : TWITTER_API_URL;
    // Adjust API path (if this is not an authentication request)
    if (!path.match(/oauth/)) {
      path = '1.1/' + path + '.json';
    }

    // Feed the parameters into the OAuth helper
    let url = baseUrl + path;
    let message = {
      action:     url,
      method:     method,
      parameters: params
    };
    OAuth.completeRequest(message, this._defaultOAuthCredentials(this.credentials));

    // Retrieve properly formatted parameters suitable for inclusion in the HTTP request
    // from the OAuth helper
    let requestParams = new URLSearchParams();
    Object.entries(OAuth.getParameterMap(message.parameters)).forEach(([key, value]) => {
      if (value == null) {
        value = '';
      }
      requestParams.append(key, value);
    });

    // Send request
    // let authorizationHeader = OAuth.getAuthorizationHeader(null, message.parameters);
    // let response = await fetch(url, { method: method, headers: { 'Authorization': authorizationHeader }});
    let response = await fetch(url, { method: method, body: requestParams });

    // Handle non 200-range response codes
    let responseText = await response.text();
    if (response.ok) {
      return responseText;
    } else {
      throw new Error(`[TwitterClient] API request at '${path}' failed (${responseText})`);
    }
  }

  // Returns an object containing OAuth credentials,
  // which can be used as the `accessor` argument of `OAuth.completeRequest`.
  //
  // OAuth credentials can still be overriden at the request level.
  _defaultOAuthCredentials(credentials) {
    return {
      consumerKey:    this.consumer_key,
      consumerSecret: this.consumer_secret,
      token:          credentials && credentials.oauth_token,
      tokenSecret:    credentials && credentials.oauth_token_secret
    };
  }
}
