/**
 * This script is run into the extension permanent page.
 * It receives messages from the content web pages, and reacts accordingly.
 */

// FIXME: de-duplicate this info with the content page
const TWITTER_BIRDSITE_CONSUMER_KEY    = '9Y6TkkJkq65aBTi07ozaNYgP7';
const TWITTER_BIRDSITE_CONSUMER_SECRET = 'NhVLcbe4WD2rGUHxRUsdhCvLFIkjqWHqrkFIIYQ0sXV5Zo4R7w';

let gTwitterApi = new TwitterApi();

// When bootstrap.js detects the Mastodon web app,
// inject the extension scripts into the Mastodon page.
async function injectContentScripts(request, sender) {
  try {
    let tabId = sender.tab.id;
    // Load order-independant scripts all at once...
    // (because it is faster than loading them one-by-one)
    await Promise.all([
      chromeTabsInsertCSS(tabId, { file: "/css/birdsite.css", runAt: 'document_end' }),
      chromeTabsExecuteScript(tabId, { file: "/js/lib/promisified_chrome.js", runAt: 'document_end' }),
      chromeTabsExecuteScript(tabId, { file: "/js/content_scripts/lib/twitter-text.js", runAt: 'document_end' }),
      chromeTabsExecuteScript(tabId, { file: "/js/content_scripts/lib/twitter_client.js", runAt: 'document_end' }),
      chromeTabsExecuteScript(tabId, { file: "/js/content_scripts/lib/mastodon_client.js", runAt: 'document_end' }),
      chromeTabsExecuteScript(tabId, { file: "/js/content_scripts/mastodon/birdsite_ui.js", runAt: 'document_end' })
    ]);

    // Initialize API credentials
    let credentials = await (TwitterCredentials.load());
    gTwitterApi.setCredentials(credentials);

    // ...then load the final script that will run the extension.
    await chromeTabsExecuteScript(tabId, { file: "/js/content_scripts/mastodon/birdsite.js", runAt: 'document_end' });

  } catch (error) {
    console.error(error);
    console.error('BirdSite: error while injecting scripts: ' + error);
  }
}

// Retrieve the Twitter username from the saved credentials.
//
// If no saved credentials are present, returns null.
function getTwitterCredentialsUsername(request, sender, sendResponse) {
  let credentials = gTwitterApi.credentials;
  if (credentials) {
    sendResponse({ username: credentials.screen_name, errorMsg: null });
  } else {
    sendResponse({ username: null, errorMsg: null });
  }
}

// Save Twitter credentials to the local storage, and start
// using them in the Twitter API requests.
async function setTwitterCredentials(request, sender, sendResponse) {
  try {
    let credentials = new TwitterCredentials(request.credentialsAttrs);
    await (credentials.save());

    gTwitterApi.setCredentials(credentials);

    sendResponse({ success: true, errorMsg: null });

  } catch (error) {
    sendResponse({ success: false, errorMsg: error.message });
  }
}

// Send a Twitter API request (using the saved credentials if present).
function sendTwitterApiRequest(request, sender, sendResponse) {
  gTwitterApi.sendRequest(request.path, request.method, request.params)
    .then(responseText => sendResponse({ responseText: responseText, errorMsg: null }))
    .catch(error => sendResponse({ responseText: null, errorMsg: error.message }));
}

// Wait for the 'trigger_authentication_callback' message to be received.
// When the message is received, the sendResponse callback is called with
// the authentication callback data.
let gPendingAuthenticationCallback = null;
function waitForAuthenticationCallback(request, sender, sendResponse) {
  gPendingAuthenticationCallback = sendResponse;
}

// When the Twitter authentication pop-up redirects to our callback page,
// forward the call to the Twitter client in the page content script.
function triggerAuthenticationCallback(request, sender) {
  if (!gPendingAuthenticationCallback) {
    console.warning('[BirdSite] Background script: auth callback message received, but nothing is waiting for the callback.');
    return;
  }

  gPendingAuthenticationCallback(request.queryParams);
  gPendingAuthenticationCallback = null;
}

// Initialize the listener for messages sent by the content scripts
chrome.runtime.onMessage.addListener(function(request, sender, sendResponse) {
  let waitForSendResponse = false;

  if (request.type == 'inject_content_scripts') {
    injectContentScripts(request, sender);

  } else if (request.type == 'get_twitter_credentials_username') {
    getTwitterCredentialsUsername(request, sender, sendResponse);

  } else if (request.type == 'set_twitter_credentials') {
    setTwitterCredentials(request, sender, sendResponse);
    waitForSendResponse = true;

  } else if (request.type == 'send_twitter_api_request') {
    sendTwitterApiRequest(request, sender, sendResponse);
    waitForSendResponse = true;

  } else if (request.type == 'wait_for_authentication_callback') {
    waitForAuthenticationCallback(request, sender, sendResponse);
    waitForSendResponse = true;

  } else if (request.type == 'trigger_authentication_callback') {
    triggerAuthenticationCallback(request, sender);

  } else {
    console.warning(`Unhandled message (${JSON.stringify(request)})`);
  }

  return waitForSendResponse;
});
