/* MIT License
 *
 * Copyright (c) 2017 Andy Jiang, Pierre de La Morinerie
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

// Manage the authentication flow and authenticated requests
// against the Twitter API.
//
// Usage:
//   let client = new TwitterClient(<your app consumer key>, <your app secret>);
//   let username = await client.getAuthenticatedUsername();
//   if (!username) {
//     username = await client.authenticate();
//   }
//   await client.sendTweet('Tweet!');
class TwitterClient {
  constructor(consumerKey, consumerSecret) {
    this.consumer_key    = consumerKey;
    this.consumer_secret = consumerSecret;
    this.username = null;
  }

  isAuthenticated() {
    return username != null;
  }

  // Returns a promise that will be resolved with the Twitter username if credentials are found,
  // and `null` otherwise.
  async getAuthenticatedUsername() {
    let { username, errorMsg } = await chromeRuntimeSendMessage({ type: 'get_twitter_credentials_username' });
    if (errorMsg) {
      throw new Error(errorMsg);
      return;
    }

    return username;
  }

  // Send any request to the Twitter API, through the background extension script.
  //
  // NB: the request has to be done on the background extension script, because Chrome
  // blocks some CROS reads made from the content page.
  // See https://www.chromium.org/Home/chromium-security/extension-content-script-fetches
  async api(path, method = 'GET', params = {}) {
    let { responseText, errorMsg } = await chromeRuntimeSendMessage({ type: 'send_twitter_api_request', path, method, params});
    if (errorMsg) {
      throw new Error(errorMsg);
      return;
    }

    return responseText;
  }

  // Start the OAuth flow, and return a promise that will resolve at the end of the flow.
  //
  // Note: this method will open a pop-up window. To avoid it being blocked by pop-up blockers,
  // call it in the same event loop than the `click` event.
  async authenticate() {
    // Open an empty pop-up window as soon as possible (to avoid pop-up blockers)
    let popup = window.open('about:blank', 'Authenticating with Twitter…');
    if (!popup) {
      throw new Error('[TwitterClient] The pop-up window was blocked. Try disabling the pop-up blocker of your browser.');
    }

    try {
      // Clear existing credentials
      await this._clearCredentials();

      // Retrieve a request token
      let responseText = await this.api('oauth/request_token', 'POST', { 'oauth_callback': 'https://joinmastodon.org/' }),
          responseParams = this._extractQueryParams(responseText);

      // Load the authentication URL into the pop-up window to start the OAuth flow
      let url = 'https://api.twitter.com/oauth/authenticate?oauth_token=' + responseParams.oauth_token;
      popup.location.href = url;

      // Listen for the completion sent by the background page
      let callbackQueryParams = await chromeRuntimeSendMessage({ type: 'wait_for_authentication_callback' }),
          callbackParams = this._extractQueryParams(callbackQueryParams);

      // Retrieve an access token from the OAuth completion params
      let accessTokenText = await this.api('oauth/access_token', 'POST', callbackParams),
          accessTokenParams = this._extractQueryParams(accessTokenText);

      // Save the access token and screen name
      this._setCredentials(accessTokenParams);

      // Return the user name
      return this.getAuthenticatedUsername();

    } catch (error) {
      if (popup) {
        popup.close();
      }
      throw new Error('[TwitterClient] Error during the OAuth flow: ' + error);
    }
  }

  // Send a file upload request to the Twitter API.
  // Returns a promise that resolves when the request finishes.
  async uploadMedia(mediaUrl) {
    // Download the media file from the external URL
    let mediaResponse = await fetch(mediaUrl);
    let mediaType = mediaResponse.headers.get("content-type");
    let mediaBlob = await mediaResponse.blob();

    // Convert the media to base64
    let mediaBase64 = await new Promise((resolve, reject) => {
      let fileReader = new window.FileReader();
      fileReader.readAsDataURL(mediaBlob);
      fileReader.onload = function() {
        let base64Url = fileReader.result;
        let base64Content = base64Url.split(',')[1];
        resolve(base64Content);
      };
      fileReader.onerror = function(error) { reject(error); };
    });

    // Initialize the media upload
    let initResponse = await this.api('media/upload', 'POST', {
      command: 'INIT',
      total_bytes: mediaBlob.size,
      media_type: mediaType
    });
    let initData = JSON.parse(initResponse);
    let mediaId = initData['media_id_string'];

    // Send data
    let appendResponse = await this.api('media/upload', 'POST', {
      command: 'APPEND',
      media_id: mediaId,
      media_data: mediaBase64,
      segment_index: 0
    });

    // Finalize download
    let finalizeResponse = await this.api('media/upload', 'POST', {
      command: 'FINALIZE',
      media_id: mediaId
    });
    return finalizeResponse;
  }

  // Use the Twitter API to post a new tweet.
  // The text will be truncated to the maximum length if needed.
  // Usage: twitterClient.postTweet(new Tweet('My status'));
  async sendTweet(tweet) {
    let mediaIds = [];
    if (tweet.medias.length > 0) {
      // Upload all medias in parallel
      let uploads = tweet.medias.map(media => this.uploadMedia(media.url));
      let mediaResponses = await Promise.all(uploads);
      mediaIds = mediaResponses
        .map(responseText => JSON.parse(responseText)['media_id_string']);
    }

    let response = await this.api('statuses/update', 'POST', {
      status: tweet.truncatedText(),
      media_ids: mediaIds.join(',')
    });
    return response;
  }

  // Clear user credentials.
  // Returns a promise that will resolve then the credentials have been removed from the persistent store
  async logout() {
    this._clearCredentials();
  }

  /* Private methods */

  async _clearCredentials() {
    await this._setCredentials({});
  }

  async _setCredentials(credentialsAttrs) {
    let { success, errorMsg } = await chromeRuntimeSendMessage({ type: 'set_twitter_credentials', credentialsAttrs });
    if (errorMsg) {
      throw new Error(errorMsg);
      return;
    }

    return success;
  }

  // Convert a query string into an key-value object
  _extractQueryParams(responseText) {
    let result = {};
    responseText.split('&').forEach((param) => {
      let pair = param.split('=');
      result[pair[0]] = pair[1];
    });
    return result;
  }
}

// Represent a tweet to be posted.
// Uses twitter-text.js (aliased as window.twttr)
class Tweet {
  get MAX_TWEET_LENGTH() { return 280; }

  constructor(text) {
    this.text = text;
    this.externalUrl = null;
    this.medias = [];
    this.twitterText = window.twttr.txt;
  }

  // Set an URL to the full content, which will be added if the tweet needs to be truncated.
  setExternalUrl(url) {
    this.externalUrl = url;
  }

  addMedia(url, type) {
    this.medias.push({ url, type });
  }

  hasPattern(regexp) {
    return !!this.text.match(regexp);
  }

  deletePattern(regexp) {
    this.text = this.text.replace(regexp, '');
  }

  needsTruncation() {
    return this.twitterText.getTweetLength(this.text) > this.MAX_TWEET_LENGTH;
  }

  // Truncate the text to a size fitting in a tweet.
  //
  // When the text is already short enough, the full text is used.
  // But when the text needs to be truncated, an external URL to the full content is appended (if provided).
  truncatedText() {
    let text = this.text;

    if (! this.needsTruncation()) {
      return text;

    } else {
      let suffix = '…' + (this.externalUrl ? ` ${this.externalUrl}` : ''),
          truncatedText = text;
      for (let length = text.length; this.twitterText.getTweetLength(truncatedText) >= this.MAX_TWEET_LENGTH; length--) {
        truncatedText = text.slice(0, length) + suffix;
      }
      return truncatedText;
    }
  }

}
