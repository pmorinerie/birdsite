// A very simple client to interact with Mastodon public API.
class MastodonClient {
  // Retrieve the RSS feed of an user public toots.
  // Returns an array of statuses.
  async fetchPublicStatuses(instanceDomain, username) {
    let feedUrl = `https://${instanceDomain}/users/${username}.rss`;
    let response = await fetch(feedUrl, {cache: "reload"});
    let feed = await response.text();

    let parser = new DOMParser();
    let feedDoc = parser.parseFromString(feed, "text/xml");
    let items = feedDoc.documentElement.querySelectorAll('item');

    // Reconstruct objects like the `Status` API entity from the RSS item
    let statuses = this._mapNodes(items, item => (
      {
        id:      item.querySelector('guid').textContent.split('/').pop(),
        url:     item.querySelector('link').textContent,
        content: item.querySelector('description').textContent,
        media_attachments: this._mapNodes(item.querySelectorAll('enclosure'), enclosure => (
          {
            url:       enclosure.getAttribute('url'),
            mime_type: enclosure.getAttribute('type')
          }
        ))
      }
    ));
    return statuses;
  }

  // Retrieve the public status for a given toot text, by scanning
  // the public RSS feed for items matching the text.
  //
  // Toots in the RSS feed (or Mastodon API) will be pre-rendered with some
  // HTML, so the function attempts to match toot words one-by-one.
  //
  // Returns a `Status` object with a structure similar to the Mastodon API.
  async fetchStatusForToot(instance, username, toot) {
    let delay = 2 * 1000; // delay between each attempt (2 seconds by default)
    let timeout = 60 * 1000; // time before giving up (60 seconds by default)

    let matchingStatus = null;
    let expirationTime = Date.now() + timeout;
    while (!matchingStatus && (Date.now() < expirationTime)) {
      let publicStatuses = await this.fetchPublicStatuses(instance, username);
      matchingStatus = publicStatuses.find(status => this._fuzzyMatches(toot, status.content));
      if (matchingStatus) {
        break;
      } else {
        await new Promise(resolve => setTimeout(resolve, delay));
        continue;
      }
    }

    if (matchingStatus) {
      return matchingStatus;
    } else {
      throw new Error(`[MastodonClient] Couldn’t find a status in user '${username}' RSS feed matching '${toot}'`);
    }
  }

  /* Helpers *******************************/

  // Tells if an HTML-rendered toot matches the toot's raw text.
  // Example:
  //  _fuzzyMatch('Some toot', '<p>Some <span>toot</span></p>') -> true
  //  _fuzzyMatch('Some toot', '<p>Another <a href="#">text</a></p>') -> false
  _fuzzyMatches(rawToot, htmlToot) {
    let decodedHtmlToot = this._decodeHtmlEntities(htmlToot),
        escapedRawToot = rawToot.replace(/[.*+?^${}()|[\]\\]/g, "\\$&"),
        words = escapedRawToot.match(/\b(\w+)\b/g),
        regex = new RegExp(words.join('.*?'), 'i');
    return (decodedHtmlToot.match(regex) != null);
  }

  // Take a string with HTML entities, and return a string with the HTML entities decoded.
  _decodeHtmlEntities(encodedString) {
    let entities = {
      '&amp;':  '&',
      '&gt;':   '>',
      '&lt;':   '<',
      '&quot;': '"',
      '&apos;': "'"
    };

    let decodedString = encodedString;
    Object.entries(entities).forEach(([key, value]) => {
      decodedString = decodedString.replace(new RegExp(key, 'g'), value);
    });
    return decodedString;
  }

  // Allow using `map` on a NodeList
  _mapNodes(nodeList, callback) {
    return Array.prototype.map.call(nodeList, callback);
  }
}

