// Represents, save and retrieve Twitter user credentials from local storage.
// For internal use only.
class TwitterCredentials {
  constructor(credentials) {
    credentials = credentials || {};

    this.oauth_token = credentials.oauth_token || null;
    this.oauth_token_secret = credentials.oauth_token_secret || null;
    this.screen_name = credentials.screen_name || null;
  }

  get isComplete() {
    return this.oauth_token && this.oauth_token_secret && this.screen_name;
  }

  static async load() {
    let results = await chromeStorageLocalGet(['oauth_token', 'oauth_token_secret', 'screen_name']);
    let isComplete = results.oauth_token && results.oauth_token_secret && results.screen_name;
    if (isComplete) {
      return new TwitterCredentials(results);
    } else {
      return null;
    }
  }

  async save() {
    await chromeStorageLocalSet({
      oauth_token:        this.oauth_token,
      oauth_token_secret: this.oauth_token_secret,
      screen_name:        this.screen_name
    });
  }
}
